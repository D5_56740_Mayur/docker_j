package com.app.accounts;



public class Account {
private String accNumber;
private String name;
private double balance;
private String accType;

public Account(String accNumber) {
	super();
	this.accNumber = accNumber;
}

public Account(String accNumber, String name, double balance, String accType) {
	super();
	this.accNumber = accNumber;
	this.name = name;
	this.balance = balance;
	this.accType = accType;
}


public String getAccNumber() {
	return accNumber;
}

public void setAccNumber(String accNumber) {
	this.accNumber = accNumber;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public double getBalance() {
	return balance;
}

public void setBalance(double balance) {
	this.balance = balance;
}

public String getAccType() {
	return accType;
}

public void setAccType(String accType) {
	this.accType = accType;
}

@Override
public String toString() {
	return "Account [accNumber=" + accNumber + ", name=" + name + ", balance=" + balance + ", accType=" + accType + "]";
}

@Override
public boolean equals(Object o) {
	System.out.println("in vehicle's equals method");
	if (o instanceof Account)
		return this.accNumber.equals(((Account) o).accNumber);// String's equals method for : checking content
															// equality
	return false;// => incompatible types !!!!!
}
}
