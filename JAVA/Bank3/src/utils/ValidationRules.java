package utils;

import java.util.ArrayList;

import com.app.accounts.Account;

import custom_exceptions.AccountHandlingException;

public class ValidationRules {

	
	public static Account findByNo(String accNumber, ArrayList<Account> vehicles) throws AccountHandlingException {
		
		Account v1 = new Account(accNumber);
		int index = vehicles.indexOf(v1);
		if (index == -1)
			throw new AccountHandlingException("Account not found : Please Enter the Correct one!!!!!");
		
		return vehicles.get(index);
	}
}
