package utils;

//A Java program generate a random AlphaNumeric String
//using Regular Expressions method
import java.util.*;
import java.nio.charset.*;
public class AlphaNumString {
public static String getAlphaNumericString(int n)
{
//length declaration
byte[] array = new byte[256];
new Random().nextBytes(array);
String randomString
= new String(array, Charset.forName("UTF-8"));
//Creating a StringBuffer
StringBuffer ra = new StringBuffer();
//remove all spacial char
String AlphaNumericString
= randomString
.replaceAll("[^0-9]", "");
//Append first 20 alphanumeric characters
//from the generated random String into the result
for (int k = 0; k < AlphaNumericString.length(); k++) {
if (Character.isLetter(AlphaNumericString.charAt(k))
&& (n > 0)
|| Character.isDigit(AlphaNumericString.charAt(k))
&& (n > 0)) {
ra.append(AlphaNumericString.charAt(k));
n--;
}
}
// returning the resultant string
return ra.toString();
}
}