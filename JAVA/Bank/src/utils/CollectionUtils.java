package utils;

import java.text.ParseException;
import java.util.ArrayList;

import com.app.accounts.Account;

import custom_exceptions.AccountHandlingException;

public class CollectionUtils {
	public static ArrayList<Account> populateSampleData() throws ParseException, AccountHandlingException {
		ArrayList<Account> list = new ArrayList<>();
		
		
		list.add(new Account("232","Mayur",1220,"Savings"));
		list.add(new Account("233","Pranav",1220,"Salaried"));
		list.add(new Account("234","Vishal",1220,"Salaried"));
		list.add(new Account("235","Komal",1220,"Savings"));
		
		return list;
	}
}
